# Credits to
https://www.reddit.com/user/aucinc123/
# What I learned
Looking at their code has taught me that I need to sit down and sketch each problem out and really think about them before I start coding. This is a similar answer to my previous `CREDITS.md` but I really need to work on that. Next time the solution isn't immediately obvious to me I am going to sit down with a piece of paper and if I can't figure it out or if I can't come up with even somewhat of a solution then I will go to reddit. So again thank you to ***aucinc123*** on reddit!
