def getSample():
    with open('sample.txt') as f:
        sample = f.readlines()
        for i in range(len(sample)):
            sample[i] = sample[i].replace("\n", '').split()
        return sample

def getInput():
    with open('input.txt') as f:
        input = f.readlines()
        for i in range(len(input)):
            input[i] = input[i].replace('\n', '').split()
        return input

class seven_digit():
    def __init__(self, data):
        self.data = data[0:data.index('|')]
        self.sixCountList = []
        self.fiveCountList = []

    def setCodes(self):
        for data in self.data:
            if len(data) == 2:
                self.oneCode = data
            elif len(data) == 4:
                self.fourCode = data
            elif len(data) == 3:
                self.sevenCode = data
            elif len(data) == 7:
                self.eightCode = data
            else:
                if len(data) == 5:
                    self.fiveCountList.append(data)
                    # 2, 3, 5, 
                elif len(data) == 6:
                    self.sixCountList.append(data)
                    # 0, 6, 9

        self.zeroCode = [''.join(x) for x in self.sevenCode if x not in self.oneCode][0]
        for data in self.sixCountList:
            if len([''.join(x) for x in data if x not in self.fourCode]) == 2: self.nineCode = data
            if len([''.join(x) for x in data if x not in self.sevenCode]) == 4: self.sixCode = data
        for data in self.fiveCountList:
            if len([''.join(x) for x in data if not x in self.sevenCode]) == 2: self.threeCode = data
            if len([''.join(x) for x in data if not x in self.sevenCode]) == 3:
                if len([''.join(x) for x in data if not x in self.fourCode]) == 2: self.fiveCode = data
                elif len([''.join(x) for x in data if not x in self.fourCode]) == 3: self.twoCode = data
        self.twoCode = [''.join(x) for x in self.oneCode if x not in self.sixCode][0]
        self.fiveCode = [''.join(x) for x in self.oneCode if x not in self.twoCode][0]
        self.oneCode = [''.join(x) for x in self.nineCode if x not in self.threeCode][0]
        self.threeCode = [''.join(x) for x in self.fourCode if x not in self.oneCode and x not in self.twoCode and x not in self.fiveCode][0]
        self.fourCode = [''.join(x) for x in self.sixCode if x not in self.nineCode][0]
        self.sixCode = [''.join(x) for x in self.eightCode if x not in self.zeroCode and x not in self.oneCode and x not in self.twoCode and x not in self.threeCode and x not in self.fourCode and x not in self.fiveCode][0]
  
        

    def getNumber(self, dataList):
        string = ''
        for data in dataList:
            if self.zeroCode in data and self.oneCode in data and self.twoCode in data and not self.threeCode in data and self.fourCode in data and self.fiveCode in data and self.sixCode in data:
                string+='0'
            elif self.zeroCode not in data and self.oneCode not in data and self.twoCode in data and self.threeCode not in data and self.fourCode not in data and self.fiveCode in data and self.sixCode not in data:
                string+='1'
            elif self.zeroCode in data and self.oneCode not in data and self.twoCode in data and self.threeCode in data and self.fourCode in data and self.fiveCode not in data and self.sixCode in data:
                string+='2'
            elif self.zeroCode in data and self.oneCode not in data and self.twoCode in data and self.threeCode in data and self.fourCode not in data and self.fiveCode in data and self.sixCode in data:
                string+='3'
            elif self.zeroCode not in data and self.oneCode in data and self.twoCode in data and self.threeCode in data and self.fourCode not in data and self.fiveCode in data and self.sixCode not in data:
                string+='4'
            elif self.zeroCode in data and self.oneCode in data and self.twoCode not in data and self.threeCode in data and self.fourCode not in data and self.fiveCode in data and self.sixCode in data:
                string+='5'
            elif self.zeroCode in data and self.oneCode in data and self.twoCode not in data and self.threeCode in data and self.fourCode in data and self.fiveCode in data and self.sixCode in data:
                string+='6'
            elif self.zeroCode in data and self.oneCode not in data and self.twoCode in data and self.threeCode not in data and self.fourCode not in data and self.fiveCode in data and self.sixCode not in data:
                string+='7'
            elif self.zeroCode in data and self.oneCode in data and self.twoCode in data and self.threeCode in data and self.fourCode in data and self.fiveCode in data and self.sixCode in data:
                string+='8'
            elif self.zeroCode in data and self.oneCode in data and self.twoCode in data and self.threeCode in data and self.fourCode not in data and self.fiveCode in data and self.sixCode in data:
                string+='9'
            else:
                print(data)
                print(self.zeroCode)
                print(self.oneCode)
                print(self.twoCode)
                print(self.threeCode)
                print(self.fourCode)
                print(self.fiveCode)
                print(self.sixCode)
                assert False, "Unreachable number"
        return int(string)


answer = 0
#    000
#   1   2
#   1   2
#   1   2
#    333
#   4   5
#   4   5
#   4   5
#    666

key = []

input = getInput()

for dataLine in input:
    a = seven_digit(dataLine)
    a.setCodes()
    answer += a.getNumber(dataLine[dataLine.index('|')+1:])
print(answer)
