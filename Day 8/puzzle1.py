def getSample():
    with open('sample.txt') as f:
        sample = f.readlines()
        for i in range(len(sample)):
            sample[i] = sample[i].replace("\n", '').split()
        return sample

def getInput():
    with open('input.txt') as f:
        input = f.readlines()
        for i in range(len(input)):
            input[i] = input[i].replace('\n', '').split()
        return input

answer = 0
input = getInput()
for dataLine in input:
    for i in range(dataLine.index("|")+1, len(dataLine)):
        if len(dataLine[i]) == 2:
            # Number 1
            answer += 1
        elif len(dataLine[i]) == 4:
            # Number 4
            answer += 1
        elif len(dataLine[i]) == 3:
            # Number 7
            answer += 1
        elif len(dataLine[i]) == 7:
            # Number 8
            answer += 1
        else:
            continue
print(answer)
