﻿# Description 
This is a repo to hold all my code for Advent of Code 2021. I will primarily be using python.

# About Me
At the time of working on this I am currently 15 y/o. I know Python, C#, Java and Lua enough to work in them all. I am familiar with HTML/CSS, and JavaScript as well but I am not practiced in them. I have been on and off of coding
for a while now but this year/summer I have been really getting into it and enjoying it. I applied for the Career Center in my area for App/Software Development and Cyber Security recently. Please star this repo to see
how my brain thinks to solve these problems in my first true year of coding!

# Dependencies

## Required
#### Python
 The majority of my code is in python so you will need python to run them and get the solutions

[Python Download](https://www.python.org/ "Offical Python website")

# Credits
Inside of each day, if I could not solve the problem on my own and needed assistance from others then I credit them in a `CREDITS.md` file inside each day along with what they taught me. I will not just steal answers however so
I try to understand their code and recreate the base concept of it without directly copying them
