def getInput():
    with open("input.txt") as f:
        input = f.readlines()
        for i in range(len(input)):
            input[i] = input[i].replace('\n', '')
    return input

def getSample():
    with open("sample.txt") as f:
        input = f.readlines()
        for i in range(len(input)):
            input[i] = input[i].replace('\n', '')
    return input


low_points = []

input = getInput()
for i in range(len(input)):
    if i == 0:
        for j in range(len(input[i])):
            num = input[i][j]
            if j == 0:
                if int(num) < int(input[i][j+1]) and int(num) < int(input[i+1][j]):
                    low_points.append(num)
            elif j == len(input[i])-1:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i+1][j]):
                    low_points.append(num)
            else:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i][j+1]) and int(num) < int(input[i+1][j]):
                    low_points.append(num)
    elif i == len(input)-1:
        for j in range(len(input[i])):
            num = input[i][j]
            if j == 0:
                if int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]):
                    print('test')
                    low_points.append(num)
            elif j == len(input[i])-1:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i-1][j]):
                    print('test1')
                    low_points.append(num)
            else:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]):
                    low_points.append(num)
    else:
        for j in range(len(input[i])):
            num = input[i][j]
            if j == 0:
                if int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]) and int(num) < int(input[i+1][j]):
                    low_points.append(num)
            elif j == len(input[i])-1:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i-1][j]) and int(num) < int(input[i+1][j]):
                    low_points.append(num)
            else:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]) and int(num) < int(input[i+1][j]):
                    low_points.append(num)

danger_values = [int(x)+1 for x in low_points]
answer = sum(danger_values)

print(answer)
