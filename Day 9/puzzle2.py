def getInput():
    with open("input.txt") as f:
        input = f.readlines()
        for i in range(len(input)):
            input[i] = input[i].replace('\n', '')
    return input


def getSample():
    with open("sample.txt") as f:
        input = f.readlines()
        for i in range(len(input)):
            input[i] = input[i].replace('\n', '')
    return input


def getSurroundingNum(table, start_point):
    if start_point[1] != 0 and start_point[1] != len(table[start_point[0]])-1:
        leftNum = [table[start_point[0]][start_point[1]-1],
                   (start_point[0], start_point[1]-1)]
        rightNum = [table[start_point[0]][start_point[1]+1],
                    (start_point[0], start_point[1]+1)]
    elif start_point[1] == 0:
        leftNum = None
        rightNum = [table[start_point[0]][start_point[1]+1],
                    (start_point[0], start_point[1]+1)]
    elif start_point[1] == len(table[start_point[0]])-1:
        leftNum = [table[start_point[0]][start_point[1]-1],
                   (start_point[0], start_point[1]-1)]
        rightNum = None

    if start_point[0] != 0 and start_point[0] != len(table)-1:
        topNum = [table[start_point[0]-1][start_point[1]],
                  (start_point[0]-1, start_point[1])]
        bottomNum = [table[start_point[0]+1][start_point[1]],
                     (start_point[0]+1, start_point[1])]
    elif start_point[0] == 0:
        topNum = None
        bottomNum = [table[start_point[0]+1][start_point[1]],
                     (start_point[0]+1, start_point[1])]
    elif start_point[0] == len(table)-1:
        topNum = [table[start_point[0]-1][start_point[1]],
                  (start_point[0]-1, start_point[1])]
        bottomNum = None
    return (leftNum, rightNum, topNum, bottomNum)


def mult(iterator):
    final = 1
    for num in iterator:
        final *= int(num)
    return final


class basin():
    def __init__(self, start_i, start_j, value):
        self.startPoint = (start_i, start_j)
        self.size = 0
        self.value = value
        self.accounted = []

    def getRow(self, table, start_position):
        point = start_position
        # Left
        while table[point[0]][point[1]] != '9':
            if point not in self.accounted:
                self.size += 1
                self.accounted.append(point)
                self.getColumn(table, point)
                if getSurroundingNum(table, (point[0], point[1]))[0] is not None:
                    point = getSurroundingNum(
                        table, (point[0], point[1]))[0][1]
                    # print(f'Set point to {table[point[0]][point[1]]}')
                else:
                    break
            else:
                if getSurroundingNum(table, (point[0], point[1]))[0] is not None:
                    point = getSurroundingNum(
                        table, (point[0], point[1]))[0][1]
                    # print(f'Set point to {table[point[0]][point[1]]}')
                else:
                    break
        point = start_position
        # Right
        while table[point[0]][point[1]] != '9':
            if point not in self.accounted:
                self.size += 1
                self.accounted.append(point)
                self.getColumn(table, point)
                if getSurroundingNum(table, (point[0], point[1]))[1] is not None:
                    point = getSurroundingNum(
                        table, (point[0], point[1]))[1][1]
                    # print(f'Set point to {table[point[0]][point[1]]}')
                else:
                    break
            else:
                if getSurroundingNum(table, (point[0], point[1]))[1] is not None:
                    point = getSurroundingNum(
                        table, (point[0], point[1]))[1][1]
                    # print(f'Set point to {table[point[0]][point[1]]}')
                else:
                    break

    def getColumn(self, board, start_position):
        originalNum = start_position
        num = originalNum
        # Up
        # print('Up:')
        while board[num[0]][num[1]] != '9':
            # print(f'({num[0]}, {num[1]})')
            if num not in self.accounted:
                self.getRow(board, num)
                if getSurroundingNum(board, (num[0], num[1]))[2] is not None:
                    num = getSurroundingNum(board, (num[0], num[1]))[2][1]
                else:
                    break
            else:
                if getSurroundingNum(board, (num[0], num[1]))[2] is not None:
                    num = getSurroundingNum(board, (num[0], num[1]))[2][1]
                else:
                    break
        num = originalNum
        # Down
        # print('Down:')
        while board[num[0]][num[1]] != '9':
            # print(f'({num[0]}, {num[1]})')
            if num not in self.accounted:
                self.getRow(board, num)
                if getSurroundingNum(board, (num[0], num[1]))[3] is not None:
                    num = getSurroundingNum(board, (num[0], num[1]))[3][1]
                else:
                    break
            else:
                if getSurroundingNum(board, (num[0], num[1]))[3] is not None:
                    num = getSurroundingNum(board, (num[0], num[1]))[3][1]
                else:
                    break

    def getBasinSize(self, table):
        point = self.startPoint
        # print(self.startPoint)
        # Left
        # print('Left: ')
        while table[point[0]][point[1]] != '9':
            # print(table[point[0]][point[1]])
            self.getColumn(table, point)
            if getSurroundingNum(table, (point[0], point[1]))[0] is not None:
                point = getSurroundingNum(
                    table, (point[0], point[1]))[0][1]
                # print(f'Set point to {table[point[0]][point[1]]}')
            else:
                break
        point = self.startPoint
        # Right
        # print('Right: ')
        while table[point[0]][point[1]] != '9':
            # print(table[point[0]][point[1]])
            self.getColumn(table, point)
            if getSurroundingNum(table, (point[0], point[1]))[1] is not None:
                point = getSurroundingNum(
                    table, (point[0], point[1]))[1][1]
                # print(f'Set point to {table[point[0]][point[1]]}')
            else:
                break
        return self.size


low_points = []

input = getInput()
for i in range(len(input)):
    if i == 0:
        for j in range(len(input[i])):
            num = input[i][j]
            if j == 0:
                if int(num) < int(input[i][j+1]) and int(num) < int(input[i+1][j]):
                    low_points.append((num, i, j))
            elif j == len(input[i])-1:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i+1][j]):
                    low_points.append((num, i, j))
            else:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i][j+1]) and int(num) < int(input[i+1][j]):
                    low_points.append((num, i, j))
    elif i == len(input)-1:
        for j in range(len(input[i])):
            num = input[i][j]
            if j == 0:
                if int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]):
                    low_points.append((num, i, j))
            elif j == len(input[i])-1:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i-1][j]):
                    low_points.append((num, i, j))
            else:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]):
                    low_points.append((num, i, j))
    else:
        for j in range(len(input[i])):
            num = input[i][j]
            if j == 0:
                if int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]) and int(num) < int(input[i+1][j]):
                    low_points.append((num, i, j))
            elif j == len(input[i])-1:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i-1][j]) and int(num) < int(input[i+1][j]):
                    low_points.append((num, i, j))
            else:
                if int(num) < int(input[i][j-1]) and int(num) < int(input[i][j+1]) and int(num) < int(input[i-1][j]) and int(num) < int(input[i+1][j]):
                    low_points.append((num, i, j))

basins = [basin(x[1], x[2], x[0]) for x in low_points]
sizes = [int(b.getBasinSize(input)) for b in basins]
largest = []
# WRONG 557928, 704880
for i in range(3):
    largest.append(max(sizes))
    sizes.remove(max(sizes))
print(mult(largest))
