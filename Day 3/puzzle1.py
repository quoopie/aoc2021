def gammaEpsilon(array=[]):

    arrays = []
    counts = []

    for _ in range(len(array[0])):
        counts.append({'ones': 0, 'zeros': 0})

    for i in range(len(array)):
        for j in range(len(array[i])):
            location = j
            bit = array[i][j]
            arrays.append([location, bit])

    gamma = ''
    epsilon = ''

    for i in arrays:
        if i[1] == '1':
            counts[i[0]]['ones'] += 1
        else:
            counts[i[0]]['zeros'] += 1

    for dict in counts:
        if dict['ones'] > dict['zeros']:
            gamma += '1'
            epsilon += '0'
        else:
            gamma += '0'
            epsilon += '1'

    gamma = int(gamma, 2)
    epsilon = int(epsilon, 2)

    return gamma, epsilon


if __name__ == '__main__':
    input = []

    with open("input.txt") as f:
        for line in f:
            input.append(line.replace("\n", ''))

    test = gammaEpsilon(['00100',
                        '11110',
                         '10110',
                         '10111',
                         '10101',
                         '01111',
                         '00111',
                         '11100',
                         '10000',
                         '11001',
                         '00010',
                         '01010'])

    input = gammaEpsilon(input)

    print(input[0]*input[1])
