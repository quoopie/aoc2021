def getInput():
    input = []
    with open("input.txt") as f:
        for line in f:
            input.append(line.replace("\n", ''))
    return input


def oxygen(input):

    bytes = input.copy()
    i = -1

    while len(bytes) > 1:
        if i == 11:
            i = 0
        else:
            i += 1

        zeros = 0
        ones = 0
        for bite in bytes.copy():
            if bite[i] == '0':
                zeros += 1
            elif bite[i] == '1':
                ones += 1
            else:
                assert False, f'Unkown Value in byteObject, "{bite[i]}" at index {i}'

        if zeros > ones:
            for bite in bytes.copy():
                if bite[i] != '0':
                    bytes.remove(bite)
        else:
            for bite in bytes.copy():
                if bite[i] != '1':
                    bytes.remove(bite)

    # print(bytes[0])

    return int(bytes[0], 2)


def CO2(input):

    bytes = input.copy()
    i = -1

    while len(bytes) > 1:
        if i == 11:
            i = 0
        else:
            i += 1

        zeros = 0
        ones = 0
        for bite in bytes.copy():
            if bite[i] == '0':
                zeros += 1
            elif bite[i] == '1':
                ones += 1
            else:
                assert False, f'Unkown Value in byteObject, "{bite[i]}" at index {i}'

        if ones < zeros:
            for bite in bytes.copy():
                if bite[i] != '1':
                    bytes.remove(bite)
        else:
            for bite in bytes.copy():
                if bite[i] != '0':
                    bytes.remove(bite)

    return int(bytes[0], 2)


if __name__ == '__main__':

    test = ['00100',
            '11110',
            '10110',
            '10111',
            '10101',
            '01111',
            '00111',
            '11100',
            '10000',
            '11001',
            '00010',
            '01010']

    print(oxygen(getInput()))
    print(CO2(getInput()))
    print(CO2(getInput())*oxygen(getInput()))
