from Input import input

scans = []
count = 0

for i in range(len(input)):
    if i != len(input)-2:
        sum = input[i] + input[i+1] + input[i + 2]
        scans.append(sum)
    else:
        break

for i in range(1, len(scans)):
    if scans[i] > scans[i-1]:
        count += 1

print(count)
