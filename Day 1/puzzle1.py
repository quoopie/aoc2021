from Input import input

count = 0

for i in range(1, len(input)):
    if input[i] > input[i-1]:
        count += 1

print(count)
