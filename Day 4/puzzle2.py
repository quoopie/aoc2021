board_num = 0


class board:
    def __init__(self, board):
        global board_num
        board_num += 1
        self.number = board_num
        self.board = board
        self.bingo = False
        self.values = []
        for _ in range(25):
            self.values.append(False)

    '''
    BOARD LAYOUT:

    00 01 02 03 04
    05 06 07 08 09
    10 11 12 13 14
    15 16 17 18 19
    20 21 22 23 24

    '''

    def checkNumber(self, num):
        for i in range(25):
            if self.board[i] == num:
                self.values[i] = True

    def checkBingo(self):
        for i in range(5):
            if self.values[i] and self.values[i+5] and self.values[i+10] and self.values[i+15] and self.values[i+20]:
                self.bingo = True
                return True, self.board[i], self.board[i+5], self.board[i+10], self.board[i+15], self.board[i+20]
        for i in range(0, 25, 5):
            if self.values[i] and self.values[i+1] and self.values[i+2] and self.values[i+3] and self.values[i+4]:
                self.bingo = True
                return True, self.board[i], self.board[i+1], self.board[i+2], self.board[i+3], self.board[i+4]
        return False, 0

    def getUnmarked(self):
        ans = []
        for i in range(25):
            if not self.values[i]:
                ans.append(self.board[i])
        return ans


lines = []
for line in open("input.txt"):
    lines.append(line.replace("\n", ''))

draws = lines[0].split(',')
lines.remove(lines[0])
lines.remove('')

boards = []

for line in lines:
    if lines.count('') > 0:
        lines.remove('')
    b = []
    for i in range(5):
        for num in lines[i].split():
            b.append(num)
    for i in range(5):
        if len(lines) > 0:
            lines.remove(lines[0])
    boards.append(board(b))

bingo_board = None
winning_num = None

for draw in draws:
    if len(boards) < 2 and boards[0].bingo:
        break
    for b in boards.copy():
        b.checkNumber(draw)
        '''
        print("----------")
        print(b.number)
        print(f'Tested {draw}')
        for i in range(0, 25, 5):
            print(
                f'{b.values[i]}, {b.values[i+1]}, {b.values[i+2]}, {b.values[i+3]}, {b.values[i+4]}\n')
        print("----------")
        '''
        result = b.checkBingo()
        if result[0]:
            bingo_board = b
            winning_num = int(draw)
            if len(boards) < 2 and boards[0].bingo:
                break
            boards.remove(b)

nums = []
unmarked = bingo_board.getUnmarked()

ans = sum(map(int, unmarked))*winning_num

print(ans)
