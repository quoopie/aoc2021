from input import input

horizontal = 0
depth = 0

for movement in input:

    command = movement.split()

    if command[0] == 'forward':
        horizontal += int(command[1])
    elif command[0] == 'down':
        depth += int(command[1])
    elif command[0] == 'up':
        depth -= int(command[1])
    else:
        assert False, "Unkown command found"

print(horizontal*depth)
