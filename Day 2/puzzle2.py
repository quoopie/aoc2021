#from input import input

input = []

with open('input.txt') as f:
    for line in f:
        input.append(line.replace("\n", ''))


def dive(array):
    horizontal = 0
    depth = 0
    aim = 0

    for movement in array:

        command = movement.split()

        x = int(command[1])

        if command[0] == 'forward':
            horizontal += x
            depth += aim*x
        elif command[0] == 'down':
            aim += x
        elif command[0] == 'up':
            aim -= x
        else:
            assert False, "Unkown command found"

    return depth*horizontal


if __name__ == '__main__':
    print(dive(input))
