# Credits to for assistance:
https://www.reddit.com/user/ywgdana/
### What I learned/How they helped
###### Puzzle 2
I was brute forcing the problem originally so I was appending a new fish to a list. Instead they had a list with 9 values and added a count of fish to each index and slid them back a day/index. What I learned was that if using one
variable does not work then try to graph the problem out and see what the dependent variable is then adjust the problem accordingly. Thank you *ywgdana* on reddit for the valuable lesson!
