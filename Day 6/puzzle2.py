import pdb


days = []
days += [0]*9
startFish = []

with open("input.txt") as f:
    lines  = f.readlines()
    for line in lines:
        startFish += line.split(',')
    startFish = list(map(int, startFish))

for fishNum in startFish:
    days[fishNum] += 1

for i in range(256):
    print(f'Day: {i}')
    day0 = days.copy()[0]
    for j in range(1, len(days)):
        days[j-1] = days[j]
    days[7] = days[7]
    days[8] = 0
    days[8] += day0
    days[6] += day0
    print(days)
    print(f'Fish: {sum(days)}')
print(sum(days))
