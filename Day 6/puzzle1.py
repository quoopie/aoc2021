import pdb


lantern_fishes = []

class lantern_fish():
    def __init__(self, age):
        self.age = age

    def age_one_day(self):
         self.age -= 1

    def resetAge(self):
        self.age = 6

nums = []

with open("input.txt") as f:
    lines  = f.readlines()
    for line in lines:
        nums += line.replace("\n","").split(",")
    nums = list(map(int, nums))

for num in nums:
    lantern_fishes.append(lantern_fish(num))

ages = []

for i in range(80):
    ages.clear()
    for fish in lantern_fishes.copy():
        fish.age_one_day()
        if fish.age == -1:
            lantern_fishes.append(lantern_fish(8))
            fish.resetAge()
        ages.append(fish.age)
    #print(f'After {i} Day(s): {ages}') 
print(f'Number of fish: {len(lantern_fishes)}')
