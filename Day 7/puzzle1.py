import math


nums = []

with open("input.txt") as f:
    lines = f.readlines()
    for line in lines:
        line = map(int,line.split(','))
        for num in line:
            nums.append(num)

maximum = max(nums)
difference_sums = []

for i in range(maximum+1):
    differences = [abs(x-i) for x in nums]
    difference_sums.append(sum(differences))
print(min(difference_sums))
