import math


nums = []

with open("input.txt") as f:
    lines = f.readlines()
    for line in lines:
        line = map(int,line.split(','))
        for num in line:
            nums.append(num)

maximum = max(nums)
difference_sums = []

for i in range(maximum+1):
    differences = []
    for num in nums:
        sum_of_moves = 0
        move_counter = 0
        for j in range(abs(num-i), -1, -1):
            sum_of_moves += move_counter
            move_counter+=1
        differences.append(sum_of_moves)
    difference_sums.append(sum(differences))
print(min(difference_sums))
