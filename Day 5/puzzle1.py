import pdb


with open("sample.txt") as f:
    lines = f.readlines()
    for i in range(len(lines)):
        lines[i] = lines[i].replace("\n", '')

coord_pairs = []

for line in lines:
    line = line.split("-")
    line1 = line[0].split(',')
    line2 = line[1].split(',')
    x1,y1 = line1[0],line1[1].replace(' ', '')
    x2,y2 = line2[0].replace('> ', ''),line2[1].replace(' ','')
    coord_pairs.append([(x1,y1),(x2,y2)])

'''



..........
..........
..........
..........
..........
..........
..........
..........
..........
..........



'''

board = []

column_size = 0
row_size = 0
for coord_pair in coord_pairs:
    for coord in coord_pair:
        num1 = int(coord[0])
        num2 = int(coord[1])
        # pdb.set_trace()
        if num1 > row_size:
            row_size = num1
        if num2 > column_size:
            column_size = num2

row_size += 1
column_size += 1

def translateCoord(coord):
    index = 0
    for i in range(int(coord[1])):
        if i == 0:
            index+= row_size-1
            continue
        index += row_size
    if index == 0:
        index += int(coord[0])
    else:
        index += int(coord[0])+1
    return index

for c in range(column_size):
    for r in range(row_size):
        board.append(0)


for coord_pair in coord_pairs:
    xDif = (int(coord_pair[1][0]) - int(coord_pair[0][0]))
    yDif = (int(coord_pair[1][1]) - int(coord_pair[0][1]))
    # pdb.set_trace()
    if xDif > 1 and yDif > 1:
        #print("Diagonal line detected!")
        continue
    elif xDif < 0 and yDif < 0:
        #print("Diagonal line detected!")
        continue
    elif xDif > 0 and yDif < 0:
        #print("Diagonal line detected!")
        continue
    elif yDif > 0 and xDif < 0:
        #print("Diagonal line detected!")
        continue
    else:
        if xDif > 1:
            for i in range(int(coord_pair[0][0]), xDif+int(coord_pair[0][0])+1):
                coord = (i,int(coord_pair[0][1]))
                index = translateCoord(coord)
                if type(board[index]) == int:
                    board[index] += 1
                else:
                    assert False, "Unreachable in board x difference increment"
        elif yDif > 1:
            for i in range(int(coord_pair[0][1]), yDif+int(coord_pair[0][1])+1):
                coord = (int(coord_pair[0][0]), i)
                index = translateCoord(coord)
                if type(board[index]) == int:
                    board[index] += 1
                else:
                    assert False, "Unreachable in board y difference increment"
        if yDif < 0:
            for i in range(int(coord_pair[0][1]), yDif+int(coord_pair[0][1])-1, -1):
                coord = (int(coord_pair[0][0]), i)
                index = translateCoord(coord)
                if type(board[index]) == int:
                    board[index] += 1
                else:
                    assert False, "Unreachable in board y difference increment"
        elif xDif < 0:
            for i in range(int(coord_pair[0][0]), xDif+int(coord_pair[0][0])-1, -1):
                    coord = (i,int(coord_pair[0][1]))
                    index = translateCoord(coord)
                    if type(board[index]) == int:
                        board[index] += 1
                    else:
                        assert False, "Unreachable in board x difference increment"
        '''
        print("-------------------------")
        print(f'Difference between {coord_pair[0]} and {coord_pair[1]}')
        for i in range(column_size): 
            print(f'{board[i*10]} {board[i*10+1]} {board[i*10+2]} {board[i*10+3]} {board[i*10+4]} {board[i*10+5]} {board[i*10+6]} {board[i*10+7]} {board[i*10+8]} {board[i*10+9]}')
        print("-------------------------")
        '''

ans = 0

for value in board:
    if value >= 2:
        ans += 1

print(ans)
